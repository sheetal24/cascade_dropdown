﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Task_CheckBox_Demo.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="scripManager" runat="server"></asp:ScriptManager>
            <asp:UpdatePanel ID="updatePanel" runat="server">
                <ContentTemplate>
                    <table>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtFirstName" runat="server" PlaceHolder="FirstName"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtLastName" runat="server" PlaceHolder="LastName"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="chkOnSite" runat="server" style="text-align:center;vertical-align:middle" Text="OnSite" AutoPostBack="true" OnCheckedChanged="chkOnSite_CheckedChanged" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblRegistration_Message" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>

        </div>
    </form>
</body>
</html>
