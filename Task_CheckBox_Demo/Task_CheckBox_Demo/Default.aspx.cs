﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Task_CheckBox_Demo.DAL;
using Task_CheckBox_Demo.Models;

namespace Task_CheckBox_Demo
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            //mapping
            var userEntityObj = this.MapUserData();
            

            //submit registration
            var result = new UserDal().GetUserDataAsync(userEntityObj);
            


        }

        protected void chkOnSite_CheckedChanged(object sender, EventArgs e)
        {
            if(chkOnSite.Checked==true)
            {
                
            }
    
        }

        #region private method
        private  UserEntity MapUserData()
        {
          
                UserEntity userEntityObj = new UserEntity()
                {
                    FirstName = txtFirstName.Text,
                    LastName = txtLastName.Text,
                    OnSite=true
                   
                };
                return userEntityObj;
           
        }
#endregion
    }
}