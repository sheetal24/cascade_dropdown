﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Task_CheckBox_Demo.DAL.ORD;
using Task_CheckBox_Demo.Models;

namespace Task_CheckBox_Demo.DAL
{
    public class UserDal
    {
        #region declaration

        private UserDCDataContext dc=null;
        #endregion

        #region constructor
        public UserDal()
        {
            dc = new UserDCDataContext();
        }
        #endregion

        #region public methods
        public async Task<dynamic>GetUserDataAsync(UserEntity userEntityObj)
        {

            String message = null;
            int? Status = null;
            return await Task.Run(() =>
            {
                var getQuery =
                dc
                ?.uspSetUserData(
                    userEntityObj.UserId,
                    userEntityObj.FirstName,
                    userEntityObj.LastName,
                    userEntityObj.OnSite,
                    ref Status,
                    ref message);
                
                return getQuery;
            });
        }

#endregion
    }
}