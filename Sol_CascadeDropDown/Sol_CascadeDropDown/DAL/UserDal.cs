﻿using Sol_CascadeDropDown.DAL.ORD;
using Sol_CascadeDropDown.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Collections;

namespace Sol_CascadeDropDown.DAL
{
    public class UserDal
    {
        #region declaration

        private UserDCDataContext dc = null;
        #endregion
        #region constructor
        public UserDal()
        {
            dc = new UserDCDataContext();
        }
        #endregion
        #region private property
        public Func<tbl_Country,CountryEntity>SelectCountryData
        {
            get
            {
                return
                    (leCountryObj) => new CountryEntity()
                    {
                        Country_Name = leCountryObj.Country_Name,
                        Country_Id = leCountryObj.Country_Id
                    };
            }
        }
        public Func<tbl_State,StateEntity>SelectStateData
        {
            get
            {
                return
                    (leStateObj) => new StateEntity()
                    {
                        State_Name = leStateObj.State_Name,
                        State_Id = leStateObj.State_Id
                    };
            }
        }
        public Func<tbl_City,CityEntity>SelectCityData
        {
            get
            {
                return
                    (leCityObj) => new CityEntity()
                    {
                       City_Name=leCityObj.City_Name,
                       City_Id=leCityObj.City_Id
                    };
            }
        }
#endregion
        #region public 

        public async Task<IEnumerable<CountryEntity>>GetCountryData()
        {
            return await Task.Run(() =>
            {
                return
                dc
                ?.tbl_Countries
                ?.AsEnumerable()
                ?.Select(this.SelectCountryData)
                ?.ToList();
            });
        }

        public async Task<IEnumerable<StateEntity>>GetStateData()
        {
            return await Task.Run(() =>
            {
                return
                dc
                ?.tbl_States
                ?.AsEnumerable()
                ?.Select(this.SelectStateData)
                ?.ToList();
            });
        }
        public async Task<IEnumerable<CityEntity>>GetCityData()
        {
            return await Task.Run(() =>
            {
                return
                dc
                ?.tbl_States
                ?.AsEnumerable()
                ?.Select(this.SelectCityData)
                ?.ToList();
            });
        }
        
#endregion
    }
}