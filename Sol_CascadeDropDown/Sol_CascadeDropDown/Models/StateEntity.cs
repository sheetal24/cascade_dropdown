﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_CascadeDropDown.Models
{
    public class StateEntity
    {
        public decimal Country_Id { get; set; }

        public decimal State_Id { get; set; }

        public String State_Name { get; set; }
    }
}