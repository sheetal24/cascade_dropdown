﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_CascadeDropDown.Models
{
    public class CountryEntity
    {
        public decimal Country_Id { get; set; }

        public String Country_Name { get; set; }
    }
}