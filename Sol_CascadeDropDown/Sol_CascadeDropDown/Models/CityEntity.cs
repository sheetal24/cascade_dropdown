﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_CascadeDropDown.Models
{
    public class CityEntity
    {
        public decimal State_Id { get; set; }

        public decimal City_Id { get; set; }

        public string City_Name { get; set; }
    }
}