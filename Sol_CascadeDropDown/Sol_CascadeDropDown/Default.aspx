﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Sol_CascadeDropDown.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="scriptManager" runat="server">
            </asp:ScriptManager>

            <asp:UpdatePanel ID="updatePanel" runat="server">
                <ContentTemplate>
                   
                        <table>
                            <tr>
                                <td>
                                    <asp:DropDownList ID="ddlCountry" runat="server" DataValueField="CountryID" DataTextField="Select_Country" AutoPostBack="true" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged"></asp:DropDownList>
                                </td>
                            </tr>
                             <tr>
                                <td>
                                    <asp:DropDownList ID="ddlState" runat="server" DataValueField="StateID" DataTextField="Select_State" AutoPostBack="true" OnSelectedIndexChanged="ddlState_SelectedIndexChanged"></asp:DropDownList>
                                </td>
                            </tr>
                             <tr>
                                <td>
                                    <asp:DropDownList ID="ddlCity" runat="server" DataValueField="AddressId" DataTextField="City"></asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
